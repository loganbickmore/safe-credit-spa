# SAFE CC SPA
**Description:** create a single page app simulating a credit card application (*dont worry about collecting PII such as SSN,DOB,etc*)

# **[React/Redux Todo example  super useful](https://codesandbox.io/s/github/reactjs/redux/tree/master/examples/todos)**

# Stack
- [Babel](https://babeljs.io/)
- [Express](https://expressjs.com/)
- [GraphQL](http://graphql.org/code/#javascript)
- [MongoDB](https://docs.mongodb.com/manual/tutorial/getting-started/)
- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [Webpack](https://webpack.js.org/)

# Pages
### 0. Welcome
Bs text and a welcome message. A button to start the process.

### 1. Intake
Intake data and log in mongodb for reference
Data:
- name
- address
- phone
- email

### 2. Terms
interest rate and some made up verbiage

### 3. Verification
Display captured info and have a verify button

### 4. Results
Approved or Declined


# User Stories

- [] Create component buttons in React that can be reused on multiple pages. *ex: next/back button*
- [] Once verification is submitted you can no longer go back.
- [] State maintained in Redux and you can reinstate captured data in a page when moving back and forth.
  - [] when the agreement checkbox (terms page) is not checked you cannot go forward, only back.
- [] GraphQL should be used to access data from MongoDB.
- [] All data should persist in MongoDB (redux state should be maintained in mongodb)
  >   *   First page data is captured with unique identifier for use going forward
  >   *   Second page captures the customer’s agreement with a timestamp. This can be in the same document in MongoDB or in another document that is tied to the first via the ID
  >  *   Final page captures the disposition of the application following the same criteria above (same document or linked secondary/tertiary)


- [] Results page should display random outcome.
- [] Intake page should have a mix of required and optional fields.
