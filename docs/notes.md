# notes

### tutorials used
- [Express, React, Redux & React Router As An SPA - Youtube](https://youtu.be/ffNABrUiGDw?t=4m)
- [React + Redux Tutorial - Medium](https://medium.com/front-end-developers/react-redux-tutorial-d1f6c6652759)


### tutorials to consider:
- [crud app with redux/react](http://www.thegreatcodeadventure.com/building-a-simple-crud-app-with-react-redux-part-1/#part-1)
- [webapp using react and express](https://www.codeproject.com/articles/1067725/part-building-web-app-using-react-js-express-js)
- https://reactjs.org/tutorial/tutorial.html
- [graphql/mongodb](https://medium.com/the-ideal-system/graphql-and-mongodb-a-quick-example-34643e637e49)



## Tech Used
- React
  - https://reactjs.org/tutorial/tutorial.html
  -
- Redux
  - https://redux.js.org/
  - https://egghead.io/lessons/react-redux-the-single-immutable-state-tree
- GraphQL
  - http://graphql.org/code/#javascript
- MongoDB
- Node using Express
  - https://nodejs.org/en/
  - https://egghead.io/lessons/node-js-initialize-a-loopback-node-js-project-through-the-cli

# Pages
## 0. Welcome
Bs text and a welcome message. A button to start the process.

## 1. Intake
Intake data and log in mongodb for reference
Data:
- name
- address
- phone
- email

## 2. Terms
interest rate and some made up verbiage

## 3. Verification
Display captured info and have a verify button

## 4. Results
Approved or Declined


# User Stories

- [] Create component buttons in React that can be reused on multiple pages. *ex: next/back button*
- [] Once verification is submitted you can no longer go back.
- [] State maintained in Redux and you can reinstate captured data in a page when moving back and forth.
  - [] when the agreement checkbox (terms page) is not checked you cannot go forward, only back.
- [] GraphQL should be used to access data from MongoDB.
- [] All data should persist in MongoDB
  >   *   First page data is captured with unique identifier for use going forward
  >   *   Second page captures the customer’s agreement with a timestamp. This can be in the same document in MongoDB or in another document that is tied to the first via the ID
  >  *   Final page captures the disposition of the application following the same criteria above (same document or linked secondary/tertiary)


- [] Results page should display random outcome.
- [] Intake page should have a mix of required and optional fields.
