import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

const users = (state = [], action) => {
  if (action.type === "ADD_USER") {
    console.log(action, state);
    return [
      ...state,
      {
        id: action.id,
        user: action.user
      }
    ];
  }
  return state;
};

export default combineReducers({
  form: formReducer,
  users
});
