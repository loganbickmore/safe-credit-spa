let nextUserID = 0;
export const addUser = user => ({
  type: "ADD_USER",
  id: nextUserID++,
  user
});
