import React from "react";
import { Link, Redirect } from "react-router-dom";

import { reduxForm, Field } from "redux-form";

class Terms extends React.Component {
  state = {
    redirectToNext: false
  };
  handle = values => {
    console.log(values);
    // TODO: Add to redux store
    this.setState({ redirectToNext: true });
  };
  render() {
    if (this.state.redirectToNext) {
      return <Redirect to="/verify" />;
    }
    return <Form onSubmit={this.handle} />;
  }
}

let Form = props => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit} className="form">
      <p>
        Nec ea clita alterum copiosae, at vero autem ceteros pro, vis zril
        audiam an. Sed clita scripserit in. Suas magna definitiones id nec, vim
        perpetua consetetur ea. Ut graeci possim aperiri pro, pro in eros
        blandit. Tale expetendis ut duo, augue timeam rationibus nam an, errem
        disputationi an eum. Eu eirmod vituperata vim. Aliquam facilis
        inciderint ex mei. Expetendis interesset per at, pro ad agam iuvaret
        delectus, vel in dico audiam suavitate. His habeo vitae cu. Vis no natum
        dicit deserunt, per in omnis habemus, tale cibo reprehendunt sit in. Est
        ne ullum clita lucilius, ei his utroque accumsan electram. Ad numquam
        evertitur delicatissimi nam, cum diam delenit insolens at. In odio
        habemus praesent duo, ius ad option appetere. Noster intellegam vel in,
        te est dicam oporteat. Sale repudiare necessitatibus ne his, omnis
        noster eum ad, reque officiis sententiae ut sed. Cum impedit placerat
        conceptam ex, quo ne assum accusamus definitionem. Justo assum populo
        eos ut, eum modo ludus id. Has an meis adipisci, nam ei praesent
        tincidunt. Ad integre disputationi quo. Harum laoreet vix ei, putent
        integre vivendum vel ne. In per tale volutpat ocurreret, et meis harum
        vel. Vix et dicam mollis latine, errem vitae veniam sit at. Est cu ferri
        impetus, ad tollit tritani pri. Ad sea alterum albucius delicatissimi,
        consequat maiestatis scripserit ad ius, ea eam congue nostro legendos.
        No his option nostrum albucius. Ea viderer probatus cum, ad cibo
        constituto intellegebat qui. Id philosophia consectetuer usu, eu esse
        constituto qui. Mediocrem imperdiet adolescens pri ei, ut qui harum
        nonumy voluptaria, usu cu velit iriure definiebas. Salutatus voluptatum
        mel te, et vis malis omnesque. At nominati indoctum nam. Alia consul
        aperiri ius ei, magna liber persecuti cu has. Ea eos vivendum gloriatur
        inciderint, no nostro inimicus persequeris sed. Nam in munere vocibus
        epicurei, nam in everti interpretaris. Vim dico illum contentiones cu,
        has an liber appellantur theophrastus, vix eu nullam legere. Nec ad
        dolore eirmod necessitatibus. No fabulas albucius mel, sea minim
        perpetua ad. Duis assum ornatus eu vel, admodum periculis sed ei. Ut sed
        unum aeterno apeirian, ius sumo libris possim no, sea choro alterum
        repudiandae ei. Eum invenire repudiandae cu, sea ad quem lobortis. Te
        pro novum consequat. Nec eu esse option fierent, at sit esse altera
        putent, no aperiam posidonium sed. In graece disputationi sit, pri
        homero graeco ex. Ei pri recteque honestatis delicatissimi, an velit
        vivendum nec. Dicunt reprimique vel id, per at putant fuisset. Ne
        eruditi copiosae postulant sit, per adhuc exerci ut. Fierent assentior
        mel at. Ex facilis instructior sea, mutat quidam periculis usu eu. Quas
        nostro eligendi mel in. Falli ludus id vis, copiosae dissentiet ei vis.
        Ius viris abhorreant complectitur ea. Graeco gubergren ad eam, his cu
        sensibus molestiae mediocritatem.
      </p>

      <div className="field">
        <div className="control">
          <label className="checkbox">
            Agree to Terms
            <Field
              name="agreeToTerms"
              id="agreeToTerms"
              component="input"
              type="checkbox"
            />
          </label>
        </div>
      </div>
      <br />
      <br />
      <div className="field">
        <div className="control">
          <Link to="/">
            <button>Back</button>
          </Link>
          <button className="button is-link" type="submit">
            Next
          </button>
        </div>
      </div>
    </form>
  );
};
Form = reduxForm({
  form: "terms"
})(Form);

export default Terms;
