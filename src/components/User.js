import React from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import { reduxForm } from "redux-form";

class User extends React.Component {
  state = {
    redirectToNext: false
  };
  handle = values => {
    console.log(values);
    // TODO: Add to redux store
    this.setState({ redirectToNext: true });
  };
  render() {
    if (this.state.redirectToNext) {
      // TODO: set global variable to disable going back
      return <Redirect to="/results" />;
    }
    return <Form onSubmit={this.handle} />;
  }
}

let Form = props => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit} className="form">
      <h2>Placeholder for intake data</h2>
      <p> see console to view logged data from before</p>

      <br />
      <br />
      <div className="field">
        <div className="control">
          <Link to="/terms">
            <button>Back</button>
          </Link>
          <button className="button is-link" type="submit">
            Next
          </button>
        </div>
      </div>
    </form>
  );
};
Form = reduxForm({
  form: "terms"
})(Form);

/*<button type="submit">Submit</button> */

export default connect()(User);
