import React from "react";

const Results = () => (
  <div className="page">
    <h2 className="h2">
      {Math.random() >= 0.5 ? "Authorized :)" : "Not Authorized :("}
    </h2>
  </div>
);

export default Results;
