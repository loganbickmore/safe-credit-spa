import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

//import Footer from "./Footer";
import AddUser from "./AddUser";
import Terms from "./Terms";
import User from "./User";
import Results from "./Results";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <nav>
            <a>
              <Link to="/">Intake</Link>
            </a>
            <a>
              <Link to="/terms">Terms</Link>
            </a>
            <a>
              <Link to="/verify">Verify</Link>
            </a>
            <a>
              <Link to="/results">Results</Link>
            </a>
          </nav>
          <Route exact path="/" component={AddUser} />
          <Route path="/terms" component={Terms} />
          <Route path="/verify" component={User} />
          <Route path="/results" component={Results} />
        </div>
      </Router>
    );
  }
}
// pass button links into each route

export default App;
