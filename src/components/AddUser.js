import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import { reduxForm, Field } from "redux-form";
import { addUser } from "../actions";
class AddUser extends Component {
  state = {
    redirectToNext: false
  };
  handle = values => {
    //console.log(values);
    // Add to redux store
    this.props.dispatch(addUser(values));
    this.setState({ redirectToNext: true });
  };

  render() {
    if (this.state.redirectToNext) {
      return <Redirect to="/terms" />;
    }
    return <Form onSubmit={this.handle} />;
  }
}
let Form = props => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit} className="form">
      <div className="field">
        <div className="control">
          <Field
            name="firstName"
            component={renderField}
            type="text"
            label="First Name"
          />
        </div>
      </div>
      <div className="field">
        <div className="control">
          <Field
            name="lastName"
            component={renderField}
            type="text"
            label="Last Name"
          />
        </div>
      </div>
      <div className="field">
        <div className="control">
          <Field
            name="email"
            component={renderField}
            type="email"
            label="Email Address"
          />
        </div>
      </div>
      <div className="field">
        <div className="control">
          <Field
            name="phone"
            component={renderField}
            type="tel"
            label="Phone Number"
          />
        </div>
      </div>
      <div className="field">
        <div className="control">
          <Field
            name="dob"
            component={renderField}
            type="date"
            label="Date of birth"
          />
        </div>
      </div>
      <br />
      <br />
      <div className="field">
        <div className="control">
          <Link to="/">
            <button disabled>Back</button>
          </Link>
          <button className="button is-link" type="submit">
            Next
          </button>
        </div>
      </div>
    </form>
  );
};

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <div className="control">
      <label className="field">{label}</label>
      <input className="input" {...input} placeholder={label} type={type} />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

const validate = val => {
  const errors = {};
  if (!val.firstName) {
    // console.log("First Name is required");
    errors.firstName = "Required";
  }
  if (!val.lastName) {
    // console.log("Last Name is required");
    errors.lastName = "Required";
  }
  // email errors
  if (!val.email) {
    // console.log("email is required");
    errors.email = "Required";
  } else if (!/^.+@.+$/i.test(val.email)) {
    // console.log("email is invalid");
    errors.email = "Invalid email address";
  }
  // phone errors, not required but validated
  if (val.phone) {
    var x = !/^[+]?(1-|1\s|1|\d{3}-|\d{3}\s|)?((\(\d{3}\))|\d{3})(-|\s)?(\d{3})(-|\s)?(\d{4})$/g.test(
      val.phone
    );
    if (x) {
      // console.log("phone is invalid");
      errors.phone = "Invalid phone number";
    }
  }
  // dob errors
  // if (!val.dob) {
  //   errors.dob = "Required";
  // } else {
  if (val.dob) {
    var birthday = new Date(Date.parse(val.dob));
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    var age = Math.abs(ageDate.getUTCFullYear() - 1970);
    if (isNaN(Number(age))) {
      errors.dob = "Must be a number";
    } else if (Number(age) < 18) {
      errors.dob = "Sorry, you must be at least 18 years old";
    }
  }

  return errors;
};

Form = reduxForm({
  form: "addUser",
  validate
})(Form);

export default connect()(AddUser);
